import sys, os
import getpass
from yade import pack,export
user = getpass.getuser()
sys.path.append(os.getcwd())



def savePropData(O, TEST):
    from yade import export
    import numpy as np
    path = os.getcwd() + '/' + TEST['vtkName'] + '/'
    if (not os.path.exists(path)):
        os.mkdir(path)
    vtkExporter = export.VTKExporter(path)
    vtkExporter.exportSpheres(numLabel=O.iter, what=dict( \
        dist='b.state.pos.norm()', \
        linVelocity='b.state.vel', \
        angVelocity='b.state.angVel', \
        mass='b.state.mass', \
        mat_rand='b.material.id', \
        numOfContacts='len(b.intrs())'))


def velComputation(O, TEST):
    if (not TEST['isStarted']):
        found = False
        for b in O.bodies:
            if b.id == TEST['body1']:
                b1 = b
                found = True
        if found:
            if (b1.state.angVel[1] **2 > TEST['angvel_tol']):
                TEST['isStarted'] = True
                TEST['t_Start'] = O.dt * O.iter

    if (not TEST['isFinished']):
        found = False
        for b in O.bodies:
            if b.id == TEST['body2']:
                b2 = b
                found = True
        if found:
            if (b2.state.angVel[1] **2 > TEST['angvel_tol']):
                TEST['isFinished'] = True
                TEST['t_Finish'] = O.dt * O.iter

    if (TEST['slipped']==0):
        found = False
        for b in O.bodies:
            if b.id == TEST['body1']:
                b1 = b
                found = True
        if found:
            if (b1.state.pos[0]-TEST['initPos'] > TEST['slip_tol']):
                TEST['slipped'] = 2
            if (b1.state.pos[0]-TEST['initPos'] < -TEST['slip_tol']):
                TEST['slipped'] = 1



def dominoVelocity(TEST, GEOM):
    if (TEST['isStarted'] and TEST['isFinished']):
        vel = GEOM['spacing'] * (TEST['last_domino'] - TEST['first_domino']) / (TEST['t_Finish'] - TEST['t_Start'])
        TEST['vel'] = vel
        slipped = TEST['slipped']
        return vel, slipped
    else:
        return 0, 0

def cleanVel(TEST):
    TEST['isStarted'] = False
    TEST['isFinished'] = False
    TEST['t_Start'] = 0
    TEST['t_Finish'] = 0
    TEST['body1'] = None
    TEST['body2'] = None
    TEST['vel'] = 0
    TEST['slipped'] = 0