# Parameters of domino simulation

def set_baseline():

    # Problem geometry
    GEOM = {
        'num_dominoes':    10, # Dominos parameters
        'spacing':    0.4,
        'x_step':    0.1,
        'y_step':    0.1,
        'z_step':    0.05,
        'N_x':    2,
        'N_y':    1,
        'N_z':    20,
        'll': 10*0.4, # Box size
        'ww': 1.0,
        'hh': 0.1,
        'rad': 0.05 + 1e-8,
        'frad': 0.2
    }

    # Material

    M1 = {  # Dominoes material
        'id':    'id_Mat1',
        'young':    1e6,
        'poisson':    0.3,
        'density':    500,
        'frictionAngle': 0.0,
        'color': (0.9, 0.9, 0.9)
    }
    M2 = { # Foundation material
        'id':    'id_Mat2',
        'young':    1e6,
        'poisson':    0.3,
        'density':    1000,
        'frictionAngle': 0.3,
        'color': (0.0, 0.9, 0.9)
    }
    M3 = { # Hammer material
        'id':    'id_Mat3',
        'young':    1e6,
        'poisson':    0.3,
        'density':    1000,
        'frictionAngle': 0.0,
        'color': (0.9, 0.0, 0.0)
    }
    MAT = {
           'domino_mat': M1,
           'found_mat': M2,
           'hammer_mat': M3
           }

    TEST = {
        '2D': True,
        'symspan': 40000,
        'damping': 0.0,
        'gravity': (0,0,-10),
        'saveEvery': 100,
        'computeEvery': 10,
        'hummerVel': (10,0,0),
        'first_domino': 5,
        'last_domino': 10,
        'isStarted': False,
        'isFinished': False,
        't_Start': 0,
        't_Finish': 0,
        'body1': None,
        'body2': None,
        'angvel_tol': 10e-6,
        'slip_tol': 0.05,
        'vel': 0,
        'slipped': 0, # 0 - toppling, 1 - forward rotation, 2 - backward rotation
        'initPos': 0,
        'vtkName': 'vtk',
        'draw': False
    }
    params = GEOM, MAT, TEST
    return params