import numpy as np
from yade.utils import *

def run_pipeline(O, params):
    # This function runs the test
    from yade.wrapper import FrictMat
    #from yade.utils import box, sphere, Vector3
    from O_utils import savePropData, dominoVelocity, cleanVel
    O.load('zero.yade.gz')
    GEOM, MAT, TEST, pl = params

    # define material for all bodies(SI units):
    M1 = MAT['domino_mat']
    M2 = MAT['found_mat']
    M3 = MAT['hammer_mat']

    id_Mat1 = O.materials.append(FrictMat(young=M1['young'], poisson=M1['poisson'], density=M1['density'], frictionAngle=M1['frictionAngle']))
    id_Mat2 = O.materials.append(FrictMat(young=M2['young'], poisson=M2['poisson'], density=M2['density'], frictionAngle=M2['frictionAngle']))
    id_Mat3 = O.materials.append(FrictMat(young=M3['young'], poisson=M3['poisson'], density=M3['density'], frictionAngle=M3['frictionAngle']))

    Mat1 = O.materials[id_Mat1]
    Mat2 = O.materials[id_Mat2]
    Mat3 = O.materials[id_Mat3]

    # define engines:
    O.engines = [
        ForceResetter(),
        InsertionSortCollider([Bo1_Sphere_Aabb(), Bo1_Box_Aabb()]),
        InteractionLoop(
            [Ig2_Sphere_Sphere_ScGeom(), Ig2_Box_Sphere_ScGeom()],
            # box-box interactions do not exist in yade, so work with clumps
            [Ip2_FrictMat_FrictMat_FrictPhys()],
            [Law2_ScGeom_FrictPhys_CundallStrack()]
        ),
        NewtonIntegrator(damping=TEST['damping'], gravity=TEST['gravity']),
        PyRunner(command='savePropData(O, TEST)', iterPeriod=TEST['saveEvery'], label='savePropData'),
        PyRunner(command='velComputation(O, TEST)', iterPeriod=TEST['computeEvery'], label='velComputation'),
        # Yade damping is rather artificial thing, and everything depends on it!
    ]

    # Foundation box
    id_box = O.bodies.append(
        box((GEOM['ll'] / 2., GEOM['ww'] / 2., -GEOM['hh'] / 2.), (GEOM['ll'] / 2., GEOM['ww'] / 2, GEOM['hh'] / 2.), fixed=True, color=M2['color'], material=Mat2))

    # Dominoes
    dt = GEOM['x_step'] * GEOM['N_x']  # domino thickness
    dw = GEOM['y_step'] * GEOM['N_y']  # domino width
    dh = GEOM['z_step'] * GEOM['N_z']  # domino height
    step = [GEOM['x_step'], GEOM['y_step'], GEOM['z_step']]
    N = [GEOM['N_x'], GEOM['N_y'], GEOM['N_z']]
    for m in range(GEOM['num_dominoes']):
        x_disp = (m + 0.5) * GEOM['spacing']
        y_disp = GEOM['ww'] / 2 - dw / 2 + GEOM['rad'] / 2
        z_disp = GEOM['rad']
        clump1 = O.bodies.appendClumped(
            [sphere([x_disp, 0 + y_disp, 0 + z_disp], material=Mat2, color=M2['color'], radius=GEOM['rad'])])
        id_clump1 = clump1[0]
        l = False
        for i in range(N[0]):
            for j in range(N[1]):
                for k in range(N[2]):
                    if l:
                        if (k == 0):
                            id_new = O.bodies.append(
                                sphere([i * step[0] + x_disp, j * step[1] + y_disp, k * step[2] + z_disp],
                                       material=Mat2, color=M2['color'], radius=GEOM['rad']))
                            O.bodies.addToClump([id_new], id_clump1)
                        else:
                            id_new = O.bodies.append(
                                sphere([i * step[0] + x_disp, j * step[1] + y_disp, k * step[2] + z_disp],
                                       material=Mat1, color=M1['color'], radius=GEOM['rad']))
                            O.bodies.addToClump([id_new], id_clump1)
                        if i == N[0]-1 and j == 0 and k == 0 and m == TEST['first_domino'] - 1:
                            TEST['body1'] = id_new
                            O.bodies[-1].shape.color = Vector3(0,1,0)
                            TEST['initPos'] = O.bodies[-1].state.pos[0]
                        if i == N[0]-1 and j == 0 and k == 0 and m == TEST['last_domino'] - 1:
                            TEST['body2'] = id_new
                            O.bodies[-1].shape.color = Vector3(0, 1, 0)
                    l = True
        if (TEST['2D']): O.bodies[id_clump1].state.blockedDOFs = 'yXZ'

    # "Hummer" pushing the 1st domino         if TEST['2D']: O.bodies[id_clump1].state.blockedDOFs = 'yXZ'
    id_new = O.bodies.append(
        sphere([GEOM['spacing'] / 2. - GEOM['frad'] - GEOM['rad'], GEOM['ww'] / 2, dh], material=Mat3, fixed=False, color=M3['color'], radius=GEOM['frad']))
    O.bodies[-1].state.vel = TEST['hummerVel']  # Start moving the hummer

    # Visualization
    if TEST['draw']:
        rr = pl[0]
        rr.bgColor = Vector3(1., 1., 1.)
        rr.light1 = True
        rr.light2 = True
        rr.ghosts = False
        rr.light2Color = Vector3(0.2, 0., 0.)

    # Run
    O.dt = 0.01 * PWaveTimeStep()  # Time integration timestep
    O.run(TEST['symspan'], True)
    vel = dominoVelocity(TEST, GEOM)
    cleanVel(TEST)
    return vel
