#!/usr/bin/python
# -*- coding: utf-8 -*-

'''Example of modeling dominos in YADE'''
from __future__ import print_function

import numpy as np
import sys
import getpass
import time
from builtins import range
from yade import pack,export
user = getpass.getuser()
sys.path.append(os.getcwd())
from pipeline import run_pipeline
from baseline import set_baseline
from O_utils import *
O.save('zero.yade.gz')

# Load baseline params
GEOM, MAT, TEST = set_baseline()

# Initialize Qt objects for Qt visualization
pl = False
if TEST['draw']:
    rr = qt.Renderer()
    vv = qt.View()
    cc = qt.Controller()
    pl = [rr, vv, cc]

params = GEOM, MAT, TEST, pl

NN = 80
spacing_batch = False
vel_batch = False
slipped_batch = False
for i in range(NN):
    print("Simulation ", i, "\n")
    params[0]['spacing'] = 0.2 + 0.01*i
    params[0]['ll'] = params[0]['spacing'] * params[0]['num_dominoes'] + 5
    params[2]['vtkName'] = 'vtk_'+str(i)
    vel, slip= run_pipeline(O, params)
    if i==0:
        spacing_batch = params[0]['spacing']
        vel_batch = vel
        slipped_batch = slip
    else:
        spacing_batch = np.append(spacing_batch, params[0]['spacing'])
        vel_batch = np.append(vel_batch, vel)
        slipped_batch = np.append(slipped_batch, slip)
        np.savetxt('spacing.txt', spacing_batch, delimiter=',')
        np.savetxt('vel.txt', vel_batch, delimiter=',')
        np.savetxt('slip.txt', slipped_batch, delimiter=',')
